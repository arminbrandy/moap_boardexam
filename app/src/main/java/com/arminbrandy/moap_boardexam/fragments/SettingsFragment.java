package com.arminbrandy.moap_boardexam.fragments;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import com.arminbrandy.moap_boardexam.R;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences_general);
    }
}