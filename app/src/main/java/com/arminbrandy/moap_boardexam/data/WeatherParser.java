package com.arminbrandy.moap_boardexam.data;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WeatherParser {
    private static final String LOG_TAG = WeatherParser.class.getCanonicalName();
    public static boolean isImperial;
    private static MutableLiveData<List<WeatherItemView>> weatherItemViews = new MutableLiveData<>();

    static List<WeatherItem> parseJSONToData(String weatherJSON) {
        List<WeatherItem> weatherItems = new ArrayList<>();

        try {
            JSONObject dataObject = new JSONObject(weatherJSON);
            JSONArray wList = dataObject.getJSONArray("list");
            for (int i = 0; i < wList.length(); i++) {
                JSONObject wItem = wList.getJSONObject(i);
                JSONObject wItemMain = wItem.optJSONObject("main");
                JSONArray wItemWeatherArray = wItem.optJSONArray("weather");
                JSONObject wItemWeather = (wItemWeatherArray != null) ?
                        wItemWeatherArray.optJSONObject(0) : null;
                JSONObject wItemClouds = wItem.optJSONObject("clouds");
                JSONObject wItemWind = wItem.optJSONObject("wind");
                JSONObject wItemRain = wItem.optJSONObject("rain");
                JSONObject wItemSnow = wItem.optJSONObject("snow");

                weatherItems.add(new WeatherItem(
                        new Date((long) wItem.getInt("dt") * 1000),
                        (wItemWeather != null) ?
                                wItemWeather.optString("icon") : "-",
                        (wItemWeather != null) ?
                                wItemWeather.optString("description") : "-",
                        (wItemMain != null) ?
                                k2c(wItemMain.optDouble("temp")) : Double.NaN,
                        (wItemMain != null) ?
                                wItemMain.optDouble("pressure") : Double.NaN,
                        (wItemMain != null) ?
                                wItemMain.optDouble("humidity") : Double.NaN,
                        (wItemClouds != null) ?
                                wItemClouds.optDouble("all") : Double.NaN,
                        (wItemWind != null) ?
                                wItemWind.optDouble("speed") : Double.NaN,
                        (wItemWind != null) ?
                                wItemWind.optDouble("deg") : Double.NaN,
                        (wItemRain != null) ?
                                wItemRain.optDouble("3h") : Double.NaN,
                        (wItemSnow != null) ?
                                wItemSnow.optDouble("3h") : Double.NaN
                ));
            }
        } catch (JSONException e) {
            Log.w(LOG_TAG, "Error during JSON parsing", e);
        }
        return weatherItems;
    }

    public static LiveData<List<WeatherItemView>> parseDataToViewable(List<WeatherItem> weatherItems) {
        List<WeatherItemView> weatherItemViews = new ArrayList<>();

        for (WeatherItem weatherItem : weatherItems) {
            weatherItemViews.add(parseItemToViewable(weatherItem));
        }

        WeatherParser.weatherItemViews.setValue(weatherItemViews);
        return WeatherParser.weatherItemViews;
    }

    public static WeatherItemView parseItemToViewable(WeatherItem weatherItem) {
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM);
        return new WeatherItemView(
                df.format(weatherItem.getDate()),
                weatherItem.getIcon(),
                capitalize(weatherItem.getCondition()),
                (notNullOrNaN(weatherItem.getTemperature())) ?
                        (isImperial ?
                                c2f(weatherItem.getTemperature()) + " °F" :
                                weatherItem.getTemperature() + " °C"
                        ) : "-",
                (notNullOrNaN(weatherItem.getPressure())) ?
                        weatherItem.getPressure() + " hPa" : "-",
                (notNullOrNaN(weatherItem.getHumidity())) ?
                        weatherItem.getHumidity() + " %" : "-",
                (notNullOrNaN(weatherItem.getCloudCover())) ?
                        weatherItem.getCloudCover() + " %" : "-",
                (notNullOrNaN(weatherItem.getWindSpeed())) ?
                        (isImperial ?
                                ms2kn(weatherItem.getWindSpeed()) + " kn" :
                                weatherItem.getWindSpeed() + " m/s"
                        ) : "-",
                (notNullOrNaN(weatherItem.getWindDirection())) ?
                        weatherItem.getWindDirection() + " deg" : "-",
                (notNullOrNaN(weatherItem.getRain())) ?
                        weatherItem.getRain() + " mm" : "-",
                (notNullOrNaN(weatherItem.getSnow())) ?
                        weatherItem.getSnow() + " mm" : "-"
        );
    }

    private static boolean notNullOrNaN(Double d) {
        return (d != null) && !d.isNaN();
    }

    private static String capitalize(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /*
     *  Unit conversion helper methods:
     * */

    private static Double k2c(Double k) {
        return Math.round((k - 273.15) * 100.0) / 100.0;
    }

    public static Double c2f(Double c) {
        return Math.round(((c * 9 / 5) + 32) * 100) / 100.0;
    }

    private static Double ms2kn(Double ms) {
        return Math.round(ms * 1.943844492 * 100) / 100.0;
    }
}
