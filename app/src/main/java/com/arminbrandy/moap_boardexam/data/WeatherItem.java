package com.arminbrandy.moap_boardexam.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "weatheritem", indices = {@Index(value = {"_id"}, unique = true)})
public class WeatherItem implements Serializable {
    private static final long serialVersionUID = 1L;

    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long mId;
    @ColumnInfo
    @NonNull
    private Date mDate;
    @ColumnInfo
    private String mIcon;
    @ColumnInfo
    private String mCondition;
    @ColumnInfo
    private Double mTemperature;
    @ColumnInfo
    private Double mPressure;
    @ColumnInfo
    private Double mHumidity;
    @ColumnInfo
    private Double mCloudCover;
    @ColumnInfo
    private Double mWindSpeed;
    @ColumnInfo
    private Double mWindDirection;
    @ColumnInfo
    private Double mRain;
    @ColumnInfo
    private Double mSnow;

    public WeatherItem(
            long id,
            Date date,
            String icon,
            String condition,
            Double temperature,
            Double pressure,
            Double humidity,
            Double cloudCover,
            Double windSpeed,
            Double windDirection,
            Double rain,
            Double snow) {
        this.mId = id;
        this.mDate = date;
        this.mIcon = icon;
        this.mCondition = condition;
        this.mTemperature = temperature;
        this.mPressure = pressure;
        this.mHumidity = humidity;
        this.mCloudCover = cloudCover;
        this.mWindSpeed = windSpeed;
        this.mWindDirection = windDirection;
        this.mRain = rain;
        this.mSnow = snow;
    }

    @Ignore
    public WeatherItem(
            Date date,
            String icon,
            String condition,
            Double temperature,
            Double pressure,
            Double humidity,
            Double cloudCover,
            Double windSpeed,
            Double windDirection,
            Double rain,
            Double snow) {
        this.mDate = date;
        this.mIcon = icon;
        this.mCondition = condition;
        this.mTemperature = temperature;
        this.mPressure = pressure;
        this.mHumidity = humidity;
        this.mCloudCover = cloudCover;
        this.mWindSpeed = windSpeed;
        this.mWindDirection = windDirection;
        this.mRain = rain;
        this.mSnow = snow;
    }

    public boolean equals(WeatherItem n) {
        return (this.mDate.equals(n.mDate) &&
                equalsOrNull(this.mIcon, n.mIcon) &&
                equalsOrNull(this.mCondition, n.mCondition) &&
                equalsOrNull(this.mTemperature, n.mTemperature) &&
                equalsOrNull(this.mPressure, n.mPressure) &&
                equalsOrNull(this.mHumidity, n.mHumidity) &&
                equalsOrNull(this.mCloudCover, n.mCloudCover) &&
                equalsOrNull(this.mWindSpeed, n.mWindSpeed) &&
                equalsOrNull(this.mWindDirection, n.mWindDirection) &&
                equalsOrNull(this.mRain, n.mRain) &&
                equalsOrNull(this.mSnow, n.mSnow));
    }

    private <T> boolean equalsOrNull(T a, T b) {
        if (a != null && b != null)
            return a.equals(b);
        else
            return a == b;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    @NonNull
    public Date getDate() {
        return mDate;
    }

    public void setDate(@NonNull Date mDate) {
        this.mDate = mDate;
    }

    public String getIcon() {
        return mIcon;
    }

    public void setIcon(String mIcon) {
        this.mIcon = mIcon;
    }

    public String getCondition() {
        return mCondition;
    }

    public void setCondition(String mCondition) {
        this.mCondition = mCondition;
    }

    public Double getTemperature() {
        return mTemperature;
    }

    public void setTemperature(Double mTemperature) {
        this.mTemperature = mTemperature;
    }

    public Double getPressure() {
        return mPressure;
    }

    public void setPressure(Double mPressure) {
        this.mPressure = mPressure;
    }

    public Double getHumidity() {
        return mHumidity;
    }

    public void setHumidity(Double mHumidity) {
        this.mHumidity = mHumidity;
    }

    public Double getCloudCover() {
        return mCloudCover;
    }

    public void setCloudCover(Double mCloudCover) {
        this.mCloudCover = mCloudCover;
    }

    public Double getWindSpeed() {
        return mWindSpeed;
    }

    public void setWindSpeed(Double mWindSpeed) {
        this.mWindSpeed = mWindSpeed;
    }

    public Double getWindDirection() {
        return mWindDirection;
    }

    public void setWindDirection(Double mWindDirection) {
        this.mWindDirection = mWindDirection;
    }

    public Double getRain() {
        return mRain;
    }

    public void setRain(Double mRain) {
        this.mRain = mRain;
    }

    public Double getSnow() {
        return mSnow;
    }

    public void setSnow(Double mSnow) {
        this.mSnow = mSnow;
    }
}