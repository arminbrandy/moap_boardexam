package com.arminbrandy.moap_boardexam.data;

import java.io.Serializable;

public class WeatherItemView implements Serializable {
    private String mDateTime;
    private String mIcon;
    private String mCondition;
    private String mTemperature;
    private String mPressure;
    private String mHumidity;
    private String mCloudCover;
    private String mWindSpeed;
    private String mWindDirection;
    private String mRain;
    private String mSnow;

    public WeatherItemView(
            String datetime,
            String icon,
            String condition,
            String temperature,
            String pressure,
            String humidity,
            String cloudCover,
            String windSpeed,
            String windDirection,
            String rain,
            String snow) {
        this.mDateTime = datetime;
        this.mIcon = icon;
        this.mCondition = condition;
        this.mTemperature = temperature;
        this.mPressure = pressure;
        this.mHumidity = humidity;
        this.mCloudCover = cloudCover;
        this.mWindSpeed = windSpeed;
        this.mWindDirection = windDirection;
        this.mRain = rain;
        this.mSnow = snow;
    }

    public boolean equals(WeatherItemView n) {
        return (this.mDateTime.equals(n.mDateTime) &&
                this.mIcon.equals(n.mIcon) &&
                this.mCondition.equals(n.mCondition) &&
                this.mTemperature.equals(n.mTemperature) &&
                this.mPressure.equals(n.mPressure) &&
                this.mHumidity.equals(n.mHumidity) &&
                this.mCloudCover.equals(n.mCloudCover) &&
                this.mWindSpeed.equals(n.mWindSpeed) &&
                this.mWindDirection.equals(n.mWindDirection) &&
                this.mRain.equals(n.mRain) &&
                this.mSnow.equals(n.mSnow));
    }

    public String getDateTime() {
        return mDateTime;
    }

    public String getIcon() {
        return mIcon;
    }

    public String getCondition() {
        return mCondition;
    }

    public String getTemperature() {
        return mTemperature;
    }

    public String getPressure() {
        return mPressure;
    }

    public String getHumidity() {
        return mHumidity;
    }

    public String getCloudCover() {
        return mCloudCover;
    }

    public String getWindSpeed() {
        return mWindSpeed;
    }

    public String getWindDirection() {
        return mWindDirection;
    }

    public String getRain() {
        return mRain;
    }

    public String getSnow() {
        return mSnow;
    }
}
