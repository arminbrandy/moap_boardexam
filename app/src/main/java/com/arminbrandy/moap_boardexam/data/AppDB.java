package com.arminbrandy.moap_boardexam.data;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@TypeConverters({Converters.class})
@Database(entities = {WeatherItem.class}, version = 1, exportSchema = false)
public abstract class AppDB extends RoomDatabase {
    private static final String LOG_TAG = AppDB.class.getCanonicalName();
    private static final String DATABASE_NAME = "weather";
    private static AppDB sInstance = null;
    private static Object sLock = new Object();

    public static AppDB getInstance(Context context) {
        if (sInstance == null) {
            synchronized (sLock) {
                if (sInstance == null) {
                    Log.d(LOG_TAG, "Creating new database instance.");
                    sInstance = Room.databaseBuilder(
                            context.getApplicationContext(),
                            AppDB.class,
                            AppDB.DATABASE_NAME)
                            .build();
                }
            }
        }
        Log.d(LOG_TAG, "Returning database instance.");
        return sInstance;
    }

    public abstract WeatherItemDAO weatherItemDAO();
}
