package com.arminbrandy.moap_boardexam.data;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class DataFetcher {
    private static final String LOG_TAG = DataFetcher.class.getCanonicalName();

    public static List<WeatherItem> fetchWeatherList(String urlString) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setConnectTimeout(5000);
                if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.w(LOG_TAG, String.format(
                            "Error opening weather JSON source, Error code: %1$s",
                            urlConnection.getResponseCode()
                    ));
                    return null;
                }
                String json = inputStreamToString(urlConnection.getInputStream());
                return WeatherParser.parseJSONToData(json);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.w(LOG_TAG, String.format("Error fetching weather json source, invalid url: %1$s",
                    urlString), e);
        } catch (IOException e) {
            Log.w(LOG_TAG, "Error fetching weather data.", e);
        }
        return null;
    }

    private static String inputStreamToString(InputStream stream) throws IOException {
        InputStreamReader isReader = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder sb = new StringBuilder();
        String str;
        while ((str = reader.readLine()) != null) {
            sb.append(str);
        }
        return sb.toString();
    }
}
