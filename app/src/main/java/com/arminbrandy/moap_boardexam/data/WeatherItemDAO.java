package com.arminbrandy.moap_boardexam.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.Date;
import java.util.List;

@Dao
public interface WeatherItemDAO {
    @Query("SELECT * FROM weatheritem ORDER by mDate")
    LiveData<List<WeatherItem>> getWeatherItems();

    @Query("SELECT * FROM weatheritem WHERE mDate = :date")
    WeatherItem getPerDate(Date date);

    @Query("SELECT COUNT(*) FROM weatheritem")
    int countWeatherItems();

    @Query("DELETE FROM weatheritem WHERE mDate < :date")
    void deleteOlderThan(Date date);

    @Query("DELETE FROM weatheritem")
    void emptyDB();

    @Insert
    void insert(WeatherItem item);

    @Delete
    void delete(WeatherItem item);
}
