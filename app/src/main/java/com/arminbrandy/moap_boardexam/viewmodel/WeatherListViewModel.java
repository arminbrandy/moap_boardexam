package com.arminbrandy.moap_boardexam.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.arminbrandy.moap_boardexam.data.AppDB;
import com.arminbrandy.moap_boardexam.data.WeatherItem;
import com.arminbrandy.moap_boardexam.data.WeatherItemView;
import com.arminbrandy.moap_boardexam.data.WeatherParser;

import java.util.List;

public class WeatherListViewModel extends AndroidViewModel {
    private MutableLiveData<List<WeatherItemView>> mWeatherItemViews;
    private LiveData<List<WeatherItem>> mWeatherItems;

    WeatherListViewModel(@NonNull Application application) {
        super(application);
        this.mWeatherItems = AppDB.getInstance(getApplication()).weatherItemDAO().getWeatherItems();
        this.mWeatherItemViews = (MutableLiveData<List<WeatherItemView>>) Transformations.switchMap(
                this.mWeatherItems,
                WeatherParser::parseDataToViewable
        );
        parseDataToViewable();
    }

    public LiveData<List<WeatherItemView>> getWeatherItemViews() {
        return this.mWeatherItemViews;
    }

    public void parseDataToViewable() {
        if (this.mWeatherItems.getValue() != null)
            WeatherParser.parseDataToViewable(this.mWeatherItems.getValue());
    }
}
