package com.arminbrandy.moap_boardexam;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.arminbrandy.moap_boardexam.data.WeatherItemView;

import java.lang.reflect.Field;

public class WeatherDetailsActivity extends AppCompatActivity {
    private static final String LOG_TAG = WeatherDetailsActivity.class.getCanonicalName();
    public static final String ITEM_KEY = "item";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);

        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        MainActivity.mLocation = sharedPreferences.getString(
                getString(R.string.settings_location_key),
                getString(R.string.settings_location_default)
        ).trim();

        final WeatherItemView item = (WeatherItemView) getIntent().getSerializableExtra(ITEM_KEY);

        ((TextView) findViewById(R.id.tv_weather_details_heading)).setText(String.format(
                getResources().getString(R.string.weather_details_heading), item.getDateTime()));
        ((TextView) findViewById(R.id.tv_weather_details_condition))
                .setCompoundDrawablesWithIntrinsicBounds(
                        getResources().getDrawable(getResId(
                                "icon_" + item.getIcon(), R.drawable.class
                        ), null), null, null, null
                );
        ((TextView) findViewById(R.id.tv_weather_details_condition)).setText(item.getCondition());
        ((TextView) findViewById(R.id.tv_weather_details_temperature)).setText(item.getTemperature());
        ((TextView) findViewById(R.id.tv_weather_details_pressure)).setText(item.getPressure());
        ((TextView) findViewById(R.id.tv_weather_details_humidity)).setText(item.getHumidity());
        ((TextView) findViewById(R.id.tv_weather_details_cloud_cover)).setText(item.getCloudCover());
        ((TextView) findViewById(R.id.tv_weather_details_wind_speed)).setText(item.getWindSpeed());
        ((TextView) findViewById(R.id.tv_weather_details_wind_direction)).setText(item.getWindDirection());
        ((TextView) findViewById(R.id.tv_weather_details_rain)).setText(item.getRain());
        ((TextView) findViewById(R.id.tv_weather_details_snow)).setText(item.getSnow());
    }

    public int getResId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
