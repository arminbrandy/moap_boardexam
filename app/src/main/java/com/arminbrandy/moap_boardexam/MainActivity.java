package com.arminbrandy.moap_boardexam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.arminbrandy.moap_boardexam.bg.AppExecutors;
import com.arminbrandy.moap_boardexam.bg.MyIntentService;
import com.arminbrandy.moap_boardexam.bg.MyWorker;
import com.arminbrandy.moap_boardexam.data.AppDB;
import com.arminbrandy.moap_boardexam.data.WeatherItemDAO;
import com.arminbrandy.moap_boardexam.data.WeatherParser;
import com.arminbrandy.moap_boardexam.rv.WeatherListAdapter;
import com.arminbrandy.moap_boardexam.viewmodel.ViewModelFactory;
import com.arminbrandy.moap_boardexam.viewmodel.WeatherListViewModel;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String LOG_TAG = MainActivity.class.getCanonicalName();
    private static final String API_KEY = "1860b46603f3e95f2f560042ffe6d017";
    private static final String[] URL_ARRAY = new String[]{
            "https://api.openweathermap.org/data/2.5/forecast?q=",
            "&APPID="
    };

    private WeatherListAdapter mAdapter;
    private WeatherListViewModel mWeatherListViewModel;

    public static String mLocation;

    protected static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        instance = this;

        mAdapter = new WeatherListAdapter(clickedItem -> {
            Intent intent = new Intent(MainActivity.this, WeatherDetailsActivity.class);
            intent.putExtra(WeatherDetailsActivity.ITEM_KEY, clickedItem);
            startActivity(intent);
        });

        RecyclerView weatherList = findViewById(R.id.rv_list);
        weatherList.setAdapter(mAdapter);
        weatherList.setLayoutManager(new LinearLayoutManager(this));

        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this);

        mLocation = sharedPreferences.getString(
                getString(R.string.settings_location_key),
                getString(R.string.settings_location_default)
        ).trim();

        WeatherParser.isImperial = sharedPreferences.getBoolean(
                getString(R.string.settings_measurement_unit_key),
                getResources().getBoolean(R.bool.settings_measurement_unit_default)
        );

        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        initDB();
        setupViewModel();

        PeriodicWorkRequest myPWorkRequest =
                new PeriodicWorkRequest.Builder(MyWorker.class, 1, TimeUnit.HOURS)
                        .build();
        WorkManager.getInstance(this).enqueue(myPWorkRequest);
    }

    private void setupViewModel() {
        mWeatherListViewModel = ViewModelProviders.of(this,
                new ViewModelFactory(getApplication())).
                get(WeatherListViewModel.class);

        mWeatherListViewModel.getWeatherItemViews().observe(this, weatherItemViews -> {
            if (weatherItemViews == null) {
                Toast.makeText(MainActivity.this, R.string.toast_no_server_data,
                        Toast.LENGTH_LONG).show();
            }
            mAdapter.swapItems(weatherItemViews);
        });
    }

    private void reloadIntent() {
        Intent intent = new Intent(this, MyIntentService.class);
        intent.setAction(MyIntentService.TASK_INIT_DB);
        this.startService(intent);
    }

    private void initDB() {
        AppExecutors.getInstance().diskIO().execute(() -> {
            try {
                WeatherItemDAO weatherItemDAO = AppDB.getInstance(this).weatherItemDAO();
                if (weatherItemDAO.countWeatherItems() == 0) {
                    AppExecutors.getInstance().mainThread().execute(this::reloadIntent);
                    Log.d(LOG_TAG, "Initiate Data!");
                } else {
                    Log.d(LOG_TAG, "Data cached!");
                }
            } catch (SQLiteConstraintException ex) {
                showErrorMessage("Something is wrong with SQL");
                Log.d(LOG_TAG, "Constraint exception while inserting data.", ex);
            } catch (Exception ex) {
                showErrorMessage("Something is wrong in general");
                Log.d(LOG_TAG, "Exception while inserting data.", ex);
            }
        });
    }

    private void showErrorMessage(final String message) {
        AppExecutors.getInstance().mainThread().execute(() ->
                Toast.makeText(MainActivity.this, message,
                        Toast.LENGTH_LONG).show());
    }

    public String getUrl() {
        return getUrl(mLocation);
    }

    public static String getUrl(String location) {
        return URL_ARRAY[0] + location.trim() + URL_ARRAY[1] + API_KEY;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_action_reload:
                this.reloadIntent();
                return true;
            case R.id.menu_action_empty_cache:
                this.emptyDB();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.settings_location_key))) {
            mLocation = sharedPreferences.getString(
                    key,
                    getResources().getString(R.string.settings_location_default)
            ).trim();
            this.reloadIntent();
        } else if (key.equals(getString(R.string.settings_measurement_unit_key))) {
            WeatherParser.isImperial = sharedPreferences.getBoolean(
                    key,
                    getResources().getBoolean(R.bool.settings_measurement_unit_default)
            );
            mWeatherListViewModel.parseDataToViewable();
        }
    }

    public static Resources getResourcesHelper() {
        return instance.getResources();
    }

    public void emptyDB() {
        AppExecutors.getInstance().diskIO().execute(() -> {
            try {
                WeatherItemDAO weatherItemDAO = AppDB.getInstance(this).weatherItemDAO();
                weatherItemDAO.emptyDB();
            } catch (SQLiteConstraintException ex) {
                showErrorMessage("Something is wrong with SQL");
                Log.d(LOG_TAG, "Constraint exception while deleting data.", ex);
            } catch (Exception ex) {
                showErrorMessage("Something is wrong in general");
                Log.d(LOG_TAG, "Exception while deleting data.", ex);
            }
        });
    }
}
