package com.arminbrandy.moap_boardexam.rv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arminbrandy.moap_boardexam.MainActivity;
import com.arminbrandy.moap_boardexam.R;
import com.arminbrandy.moap_boardexam.data.WeatherItemView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.ItemViewHolder> {
    private static final String LOG_TAG = WeatherListAdapter.class.getCanonicalName();

    private List<WeatherItemView> mItems = new ArrayList<>();
    private ListItemClickListener listItemClickListener;

    public interface ListItemClickListener {
        void onListItemClick(WeatherItemView clickedItem);
    }

    public WeatherListAdapter(ListItemClickListener clickListener) {
        this.listItemClickListener = clickListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private LinearLayout mWeatherItemView;
        private TextView mWeatherItemDateTime;
        private TextView mWeatherItemCondition;
        private TextView mWeatherItemTemperature;
        private ImageView mWeatherItemIcon;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            mWeatherItemView = itemView.findViewById(R.id.list_item_layout);
            mWeatherItemDateTime = itemView.findViewById(R.id.tv_weather_item_date_time);
            mWeatherItemCondition = itemView.findViewById(R.id.tv_weather_item_condition);
            mWeatherItemTemperature = itemView.findViewById(R.id.tv_weather_item_temperature);
            mWeatherItemIcon = itemView.findViewById(R.id.iv_weather_item_icon);
            mWeatherItemView.setOnClickListener(this);
        }

        void bind(int index) {
            mWeatherItemDateTime.setText(mItems.get(index).getDateTime());
            mWeatherItemTemperature.setText(mItems.get(index).getTemperature());
            mWeatherItemCondition.setText(mItems.get(index).getCondition());
            mWeatherItemIcon.setImageResource(getResId(
                    "icon_" + mItems.get(index).getIcon(),
                    R.drawable.class));
            mWeatherItemIcon.setContentDescription(
                    MainActivity.getResourcesHelper().getString(getResId(
                            "icon_description_" + mItems.get(index).getIcon(),
                            R.string.class
                    )));
            if (index == 0) {
                mWeatherItemView.setBackgroundColor(MainActivity.getResourcesHelper().getColor(R.color.lightBg));
                mWeatherItemDateTime.setTextSize(24);
                mWeatherItemTemperature.setTextSize(28);
                mWeatherItemCondition.setVisibility(View.VISIBLE);
                mWeatherItemIcon.setPadding(0, 20, 0, 0);
            } else {
                mWeatherItemView.setBackgroundColor(MainActivity.getResourcesHelper().getColor(R.color.white));
                mWeatherItemDateTime.setTextSize(18);
                mWeatherItemTemperature.setTextSize(20);
                mWeatherItemCondition.setVisibility(View.GONE);
                mWeatherItemIcon.setPadding(0, 0, 0, 0);

            }
        }

        @Override
        public void onClick(View v) {
            int clickedItemIndex = getAdapterPosition();
            listItemClickListener.onListItemClick(mItems.get(clickedItemIndex));
        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layoutId = R.layout.weather_list_item;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(layoutId, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return (this.mItems == null) ? 0 : this.mItems.size();
    }

    public void swapItems(List<WeatherItemView> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    private int getResId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}