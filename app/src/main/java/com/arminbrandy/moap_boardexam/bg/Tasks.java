package com.arminbrandy.moap_boardexam.bg;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import com.arminbrandy.moap_boardexam.MainActivity;
import com.arminbrandy.moap_boardexam.R;
import com.arminbrandy.moap_boardexam.data.AppDB;
import com.arminbrandy.moap_boardexam.data.DataFetcher;
import com.arminbrandy.moap_boardexam.data.WeatherItem;
import com.arminbrandy.moap_boardexam.data.WeatherItemDAO;

import java.util.List;

class Tasks {
    private static final String LOG_TAG = Tasks.class.getCanonicalName();

    static void task_init_db(Context c) {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(c);

        String location = sharedPreferences.getString(
                c.getString(R.string.settings_location_key),
                c.getString(R.string.settings_location_default)
        ).trim();

        String url = MainActivity.getUrl(location);
        AppExecutors.getInstance().networkIO().execute(() -> {
            List<WeatherItem> weatherItems = DataFetcher.fetchWeatherList(url);
            AppExecutors.getInstance().mainThread().execute(() -> {
                if (weatherItems != null) {
                    for (WeatherItem item : weatherItems) {
                        dbInput(c, item);
                    }
                } else {
                    Toast.makeText(c,
                            c.getString(R.string.toast_no_server_data) + "\n" + url,
                            Toast.LENGTH_LONG).show();
                    Log.d(LOG_TAG, "Weather couldn't be updated somehow");
                }
            });
        });
    }

    static private void dbInput(Context c, WeatherItem weatherItem) {
        AppExecutors.getInstance().diskIO().execute(() -> {
            try {
                WeatherItemDAO weatherItemDAO = AppDB.getInstance(c).weatherItemDAO();
                WeatherItem old = weatherItemDAO.getPerDate(weatherItem.getDate());
                if (old == null) {
                    weatherItemDAO.insert(weatherItem);
                    notify(c, weatherItem);
                } else if (!old.equals(weatherItem)) {
                    weatherItemDAO.delete(old);
                    weatherItemDAO.insert(weatherItem);
                }
            } catch (SQLiteConstraintException ex) {
                showErrorMessage(c, "Something is wrong with SQL");
                Log.d(LOG_TAG, "Constraint exception while inserting data.", ex);
            } catch (Exception ex) {
                showErrorMessage(c, "Something is wrong in general");
                Log.d(LOG_TAG, "Exception while inserting data.", ex);
            }
        });
    }

    static private void notify(Context c, WeatherItem item) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Weather Update";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(NotificationUtils.CHANNEL_ID,
                    name, importance);
            NotificationManager notificationManager = (NotificationManager) c.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
            }
        }

        NotificationUtils.createNotification(c, item);
    }

    private static void showErrorMessage(Context c, final String message) {
        AppExecutors.getInstance().mainThread().execute(() ->
                Toast.makeText(c, message,
                        Toast.LENGTH_LONG).show());
    }
}