package com.arminbrandy.moap_boardexam.bg;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.arminbrandy.moap_boardexam.data.AppDB;

import java.util.Calendar;
import java.util.Date;

public class MyWorker extends Worker {
    public static final String LOG_TAG = MyWorker.class.getCanonicalName();

    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Intent intent = new Intent(getApplicationContext(), MyIntentService.class);
        intent.setAction(MyIntentService.TASK_INIT_DB);
        getApplicationContext().startService(intent);

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        AppDB.getInstance(getApplicationContext()).weatherItemDAO().deleteOlderThan(cal.getTime());

        return Result.success();
    }
}
