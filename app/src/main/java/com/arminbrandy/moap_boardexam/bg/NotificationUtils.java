package com.arminbrandy.moap_boardexam.bg;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.arminbrandy.moap_boardexam.R;
import com.arminbrandy.moap_boardexam.WeatherDetailsActivity;
import com.arminbrandy.moap_boardexam.data.WeatherItem;
import com.arminbrandy.moap_boardexam.data.WeatherParser;

import java.lang.reflect.Field;
import java.text.DateFormat;

class NotificationUtils {
    private static final String LOG_TAG = NotificationUtils.class.getCanonicalName();
    static final String CHANNEL_ID = "weather_channel_01";

    static void createNotification(Context context, WeatherItem weatherItem) {
        NotificationCompat.Builder notificationBuilder;

        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM);
        Double temp = weatherItem.getTemperature();
        temp = WeatherParser.isImperial ? WeatherParser.c2f(temp) : temp;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setContentTitle(df.format(weatherItem.getDate()))
                    .setContentText(context.getResources().getString(
                            R.string.notification_temperature) + temp +
                            (WeatherParser.isImperial ? " °F" : " °C"))
                    .setSmallIcon(getResId("icon_" + weatherItem.getIcon(), R.drawable.class))
                    .setAutoCancel(true);
        } else {
            notificationBuilder = new NotificationCompat.Builder(context)
                    .setContentTitle(weatherItem.getIcon())
                    .setAutoCancel(true);
        }

        Intent intent = new Intent(context, WeatherDetailsActivity.class);
        intent.putExtra(WeatherDetailsActivity.ITEM_KEY,
                WeatherParser.parseItemToViewable(weatherItem));

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addNextIntentWithParentStack(intent);

        int requestCode = (int) (weatherItem.getDate().getTime() / 1000 / 100);

        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent
                (requestCode, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.notify(requestCode, notificationBuilder.build());
        }
    }

    private static int getResId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
