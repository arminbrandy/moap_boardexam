package com.arminbrandy.moap_boardexam.bg;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

public class MyIntentService extends IntentService {
    private static final String LOG_TAG = MyIntentService.class.getCanonicalName();
    public static final String TASK_INIT_DB = "task_init_db";

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(LOG_TAG, "Intent has started!");
        if (intent.getAction().equals(TASK_INIT_DB))
            Tasks.task_init_db(getApplicationContext());
    }
}
